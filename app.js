const path = require('path');

const { Client } = require('discord.js');
const { Handler } = require('./handler');

const app = new Client({ disableEveryone: true });
const handler = new Handler(app);

handler.load(path.join(__dirname, './modules'), { app, commandHandler: handler });

app.login(process.env.TOKEN)