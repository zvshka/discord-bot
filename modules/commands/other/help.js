const { Command } = require('../../../handler');
const Utils = require('../../../Utils')

module.exports = class extends Command {
  constructor({ comHand }) {
    super('help', {
      aliases: ['alias', 'test'],
      info: `
        Show all the commands or info about a specific command
        **[]** is optional
        **<>** is required
      `,
      usage: 'help [command]',
      permLevel: 0,
      category: "Other"
    });
    this.test = comHand
  }

  run(handler, message, args, level) {
    if (!args[0]) {
      message.channel.send(
        `**Лучше использую сайт бота: http://${/*client.config.dashboard.domain*/ "W.I.P"}**`
      );
      const settings = Utils.getSettings(handler, message.guild)
      const myCommands = message.guild
        ? handler.commands.filter(
          cmd => handler.levelChache.get(cmd.permLevel) <= level
        )
        : handler.commands.filter(
          cmd =>
            handler.levelChache.get(cmd.permLevel) <= level &&
            cmd.guildOnly !== true
        );
      const commandNames = myCommands.keyArray();
      const longest = commandNames.reduce(
        (long, str) => Math.max(long, str.length),
        0
      );

      let currentCategory = "";
      let output = `= Command List =\n\n[Use ${
        settings.prefix
        }help <commandname> for details]\n`;
      const sorted = myCommands
        .array()
        .sort((p, c) =>
          p.category > c.category
            ? 1
            : p.name > c.name && p.category === c.category
              ? 1
              : -1
        );
      sorted.forEach(c => {
        const cat = c.category.toProperCase();
        if (currentCategory !== cat) {
          output += `\n== ${cat} ==\n`;
          currentCategory = cat;
        }
        output += `${settings.prefix}${c.name}${" ".repeat(
          longest - c.name.length
        )} :: ${c.info}\n`;
      });
      message.channel.send(output, { code: "asciidoc" });
    } else {
      let command = args[0]
      if (handler.commands.has(command)) {
        if (level < handler.levelChache[command.permLevel]) return;
        message.channel.send(
          `= ${command.name} = \n${command.info}\nusage::${
          command.usage
          }`,
          { code: "asciidoc" }
        );
      }
    }
  }
};
