const { Command } = require('../../../handler')
const Utils = require('../../../Utils')

module.exports = class extends Command {
    constructor() {
        super('ping', {
            aliases: [],
            info: "",
            usage: "",
            guildOnly: false,
            category: "Other",
            permLevel: "User"
        })
    }
    run(handler, message, args, level) {

    }
}