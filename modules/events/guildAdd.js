const Utils = require('../../Utils')

const { Event } = require('../../handler')

module.exports = class extends Event {
    constructor() {
        super('guildCreate')
    }

    run(depend, guild) {
        const handler = depend.commandHandler
        handler.app.setPresence({ game: { name: `${handler.settings.get("default").prefix}help | ${handler.app.guilds.size} Servers| Dashboard soon!`, type: 0 } })
    }
}