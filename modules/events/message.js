const { Event } = require('../../handler');
const Utils = require('../../Utils')

module.exports = class extends Event {
    constructor() {
        super('message')
    }

    run(depend, message) {
        if (message.author.bot) return;
        const handler = depend.commandHandler
        const settings = Utils.getSettings(handler, message.guild)
        const level = Utils.level(handler, settings, message)
        if (!message.content.startsWith(settings.prefix)) return;

        const args = message.content.slice(settings.prefix.length).trim().split(/ +/g);
        const command = args.shift().toLowerCase();

        let cmd = handler.commands.get(command) || handler.aliases.get(command)

        cmd.run(handler, message, args, level)
    }
}