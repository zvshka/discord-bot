const { Event, Dashboard } = require('../../handler');

module.exports = class extends Event {
  constructor() {
    super('ready');
  }

  async run(depend) {
    const handler = depend.commandHandler
    const client = depend.app
    if (!handler.settings.has("default")) {
      await handler.settings.set("default", handler.config.defaults);
    }

    console.log(`${depend.app.user.tag} starting`)
    handler.client.user.setPresence({ game: { name: `${handler.settings.get("default").prefix}help | ${handler.client.guilds.size} Servers| Dashboard soon!`, type: 0 } })
    for (let i = 0; i < handler.levels.length; i++) {
      const thisLevel = handler.levels[i];
      handler.levelChache.set(thisLevel.name, thisLevel.level)
    }
    const Dashboard = require('../../handler/Dashboard')

    new Dashboard(depend).init()
    //prototypes

    client.appInfo = await client.fetchApplication();
    setInterval(async () => {
      client.appInfo = await client.fetchApplication();
    }, 60000);

    String.prototype.toProperCase = function () {
      return this.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    };
  }
};
